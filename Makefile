package = roman

stack = stack

main-is = roman:exe:roman

exec = roman

# can limit which tests are run like: make test tests=MyTests
tests = All

run: build
	time $(stack) exec $(exec)

# e.g.   make format path=app/Main.hs
format:
	brittany --write-mode=inplace $(path)

# e.g.   make lint path=app/Main.hs
lint:
	hlint $(path)

clean:
	$(stack) clean

purge:
	$(stack) purge

build:
	$(stack) build --fast --ghc-options "-j4"

build-profile:
	$(stack) --work-dir .stack-work-profiling --profile build

ghci:
	$(stack) ghci --main-is $(main-is) --ghci-options='-j4 +RTS -A128m'

repl: ghci

# example of using a test pattern: make test tests="Quick Check"
test:
ifdef tests
	stack build --test --fast --ghc-options -j4  --ta '-p "$(tests)"'
else
	stack build --test --fast --ghc-options -j4
endif

test-ghci:
	$(stack) ghci -test --ghci-options='-j4 +RTS -A128m'

bench:
	$(stack) build --bench --fast --ghc-options -j4

ghcid:
	$(stack) exec ghcid -- -c "stack ghci --main-is $(main-is) --test \
	--ghci-options='-fobject-code -j4 +RTS -A128m'"

dev-deps:
	$(stack) install ghcid brittany hlint weeder

.PHONY : format lint clean purge build build-profile run ghci repl test test-ghci bench ghcid reset-db dev-deps

