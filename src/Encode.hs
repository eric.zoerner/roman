module Encode where

import           Data.Text
import           Data.Vector
import           Numeric.Natural

chars :: Vector Char
chars = fromList "IVXLCDM"

offsets :: Vector [Natural]
offsets = fromList
  [ []
  , [0]
  , [0, 0]
  , [0, 0, 0]
  , [0, 1]
  , [1]
  , [1, 0]
  , [1, 0, 0]
  , [1, 0, 0, 0]
  , [0, 2]
  ]

encodeValue :: Natural -> Natural -> [Char]
encodeValue 0     _   = []
encodeValue value mag = encodeValue rest (mag + 1) <> digits
 where
  (rest, low) = value `divMod` 10
  indices     = addBase <$> offsets ! fromIntegral low
  addBase i = i + 2 * mag
  digits = pickDigit . fromIntegral <$> indices
  pickDigit i = chars ! i

encode :: Natural -> Text
encode value = pack $ encodeValue value 0

birthdayGreeting :: Natural -> IO ()
birthdayGreeting n = putText $ fold ["Happy ", encode n, ", Wayne!  -- Eric"]

